#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import http.server
import urllib.parse
import sqlite3
import uuid

# Archivo de base de datos SQLite
DATABASE = 'shorturls.db'

# Crear la tabla si no existe
def init_db():
    conn = sqlite3.connect(DATABASE)
    c = conn.cursor()
    c.execute('''
        CREATE TABLE IF NOT EXISTS urls (
            id TEXT PRIMARY KEY,
            url TEXT NOT NULL
        )
    ''')
    conn.commit()
    conn.close()

class ShortURLHandler(http.server.SimpleHTTPRequestHandler):
    def do_GET(self):
        if self.path == '/':
            self.send_form()
        else:
            self.redirect_to_url()

    def do_POST(self):
        content_length = int(self.headers['Content-Length'])
        post_data = self.rfile.read(content_length).decode('utf-8')
        params = urllib.parse.parse_qs(post_data)
        url = params.get('url', [None])[0]

        if url:
            if not url.startswith(('http://', 'https://')):
                url = 'https://' + url
            self.store_url(url)
            self.send_form()
        else:
            self.send_error(400, 'Bad Request: Missing URL')

    def send_form(self):
        conn = sqlite3.connect(DATABASE)
        c = conn.cursor()
        c.execute('SELECT id, url FROM urls')
        rows = c.fetchall()
        conn.close()

        form_html = """
        <!DOCTYPE html>
        <html lang="en">
        <head>
            <meta charset="UTF-8">
            <title>URL Shortener</title>
        </head>
        <body>
            <h1>URL Shortener</h1>
            <form method="POST" action="/">
                <label for="url">URL to shorten:</label>
                <input type="text" id="url" name="url" required>
                <button type="submit">Shorten</button>
            </form>
            <h2>Shortened URLs</h2>
            <ul>
        """
        for key, value in rows:
            form_html += f'<li><a href="/{key}">{key}</a> - {value}</li>'
        form_html += """
            </ul>
        </body>
        </html>
        """
        self.send_response(200)
        self.send_header('Content-type', 'text/html')
        self.end_headers()
        self.wfile.write(form_html.encode('utf-8'))

    def store_url(self, url):
        conn = sqlite3.connect(DATABASE)
        c = conn.cursor()
        c.execute('SELECT id FROM urls WHERE url=?', (url,))
        row = c.fetchone()
        if row:
            return  # URL already shortened
        short_id = str(uuid.uuid4())[:6]
        c.execute('INSERT INTO urls (id, url) VALUES (?, ?)', (short_id, url))
        conn.commit()
        conn.close()

    def redirect_to_url(self):
        short_id = self.path.lstrip('/')
        conn = sqlite3.connect(DATABASE)
        c = conn.cursor()
        c.execute('SELECT url FROM urls WHERE id=?', (short_id,))
        row = c.fetchone()
        conn.close()
        if row:
            url = row[0]
            self.send_response(301)
            self.send_header('Location', url)
            self.end_headers()
        else:
            self.send_error(404, 'Not Found: URL does not exist')

def run(server_class=http.server.HTTPServer, handler_class=ShortURLHandler, port=1234):
    init_db()
    server_address = ('', port)
    httpd = server_class(server_address, handler_class)
    print(f'Starting server on port {port}...')
    httpd.serve_forever()

if __name__ == '__main__':
    run()
